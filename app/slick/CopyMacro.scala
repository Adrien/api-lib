package slick

import scala.language.higherKinds
import scala.reflect.macros.whitebox.Context
import language.experimental.macros

object CopyMacro {

  case class Field(name: String, field_type: String, isOption: Boolean, index: Int)

  def copyCaseClass[A, B](): Any = macro copyCaseClass_impl[A, B]

  def copyCaseClass_impl[A: c.WeakTypeTag, B: c.WeakTypeTag](c: Context)(): c.Expr[Any] = {
    import c.universe._

    def isOption(m: MethodSymbol): Boolean = {
      m.returnType.toString.startsWith("Option[")
    }

    val aType = implicitly[c.WeakTypeTag[A]].tpe
    val bType = implicitly[c.WeakTypeTag[B]].tpe

    val a_fields = aType.members.collect {
      case m: MethodSymbol if m.isCaseAccessor => m
    }.toList.map { m =>
      val type_string = m.returnType.toString
      Field(m.name.toString, type_string, isOption(m), m.pos.line * 10000 + m.pos.column)
    }.filter(m => m.name != "id").sortWith((f1, f2) => f1.index - f2.index < 0)

    val b_fields = bType.members.collect {
      case m: MethodSymbol if m.isCaseAccessor => m
    }.toList.map { m =>
      val type_string = m.returnType.toString
      Field(m.name.toString, type_string, isOption(m), m.pos.line * 10000 + m.pos.column)
    }.filter(m => m.name != "id")

    val b_fields_name = b_fields.map(_.name)

    val a_fields_map = a_fields.groupBy(f => f.name).mapValues(_.head)
    val b_fields_map = b_fields.groupBy(f => f.name).mapValues(_.head)

    val fieldMap = a_fields.filter { f =>
      b_fields_name.contains(f.name) &&
        b_fields_map.get(f.name).map(f.field_type == _.field_type).getOrElse(false) // test if has same type
    }

    val a_type_name = aType.finalResultType.toString
    val b_type_name = bType.finalResultType.toString
    val functionDescription = buildString(a_type_name, b_type_name, fieldMap,
      a_fields_map,
      b_fields_map)

    printf(s"CopyMacro : $aType")
    printf(functionDescription)

    val res = c.parse(functionDescription)
    c.Expr[Any](res)
  }

  private def buildString(a_type_name: String, b_type_name: String, fields: List[Field], a_fields: Map[String, Field], b_fields: Map[String, Field]): String = {
    val fieldMapping = fields.foldLeft("") { (result, f) =>

      val prefix = if (result.size > 0) ",\n" else ""
      //val defaultValue = if (f.isOption) s".orElse(to.${f.name})" else s".getOrElse(to.${f.name})"

      val value_mapping = (a_fields.get(f.name).map(_.isOption), b_fields.get(f.name).map(_.isOption)) match {
        case (Some(true), Some(true))   => s"from.${f.name}.orElse(to.${f.name})"
        case (Some(false), Some(false)) => s"from.${f.name}"
        case (Some(true), Some(false))  => s"from.${f.name}.getOrElse(to.${f.name})"
        case (Some(false), Some(true))  => s"Some(from.${f.name})"
        case _                          => ""
      }

      result + s"$prefix ${f.name} = $value_mapping"
    }

    s"""

        (from:$a_type_name, to:$b_type_name) => to.copy(
          $fieldMapping
        )

    """
  }

}
