package slick

import scala.language.higherKinds
import scala.reflect.macros.whitebox.Context
import language.experimental.macros
import scala.tools.nsc.Global
import scala.tools.nsc.Settings
import scala.tools.nsc.reporters.ConsoleReporter
import scala.reflect.internal.util.BatchSourceFile
import scala.io.Source

object TableMacro {

  case class Field(name: String, typeName: String, isOption: Boolean, index: Int)

  val settings = new Settings
  val global = Global(settings, new ConsoleReporter(settings))
  import global._

  def table[A](): Any = macro table_impl[A]

  def table_impl[A: c.WeakTypeTag](c: Context)(): c.Expr[Any] = {
    import c.universe._

    val aType = implicitly[c.WeakTypeTag[A]].tpe

    val name = aType.typeSymbol.name.toString

    val fieldMap = aType.members.collect {
      case m: MethodSymbol if m.isCaseAccessor => m
    }.toList.map { m =>
      val name = m.name.toString
      val returnType = parseTypeString(m.returnType.toString)

      Field(name, returnType._1, returnType._2, m.pos.line * 10000 + m.pos.column)
    }.filter(m => m.name != "id").sortWith((f1, f2) => f1.index - f2.index < 0)

    val componentDescription = buildString(name, fieldMap)
    println(s"TableMacro : $name")
    println(componentDescription)

    val res = c.parse(componentDescription)
    c.Expr[Any](res)
  }

  private def parseTypeString(t: String) = {
    val Pattern = "Option\\[(.*)\\]".r

    t match {
      case Pattern(x) => (x, true)
      case _          => (t, false)
    }
  }

  private def buildString(typeName: String, fields: List[Field]): String = {
    val name = typeName.toLowerCase()
    val nameCap = name.capitalize
    val plural = name + "s"
    val pluralCap = plural.capitalize
    val tableName = if (isSqlReservedWord(name)) name + "s" else name

    val idField = """def id = column[Long]("id", O.PrimaryKey, O.AutoInc)"""
    val columns = fields.foldLeft(idField) { (result, f) =>
      val additionnalOption = if (f.isOption) ", O.Nullable" else ", O.NotNull"

      result + "\n" + s"""def ${f.name} = column[${f.typeName}]("${f.name.toLowerCase}"${additionnalOption})"""
    }

    val projection = fields.foldLeft("id.?") { (result, f) =>
      val suffix = if (f.isOption) ".?" else ""

      result + ", " + s"${f.name}${suffix}"
    }

    s""" {
        class ${pluralCap}(tag: Tag) extends Table[${nameCap}](tag, "${tableName}") {
          ${columns}
          def * = (${projection}) <> (${nameCap}.tupled, ${nameCap}.unapply)
        }

        val tableQ = TableQuery[${pluralCap}]
        tableQ
      }
      """
  }

  private def isSqlReservedWord(word: String) = {
    word match {
      case "user" => true
      case _      => false
    }
  }
}
