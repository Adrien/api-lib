package api

import play.api.Play
import play.api.libs.ws._
import api.filters.FilterUtils

object ApiUtils {

  def url(path: String)(implicit app: play.api.Application): WSRequest = {
    val apiKey = Play.configuration.getString("api.key").getOrElse("")
    val apiSecret = Play.configuration.getString("api.secret").getOrElse("")
    val today = FilterUtils.buildDateHeader()

    WS.url(path).withHeaders(
      ("Date", today),
      ("Authorization", FilterUtils.buildAuthorizationHeader(today, apiKey, apiSecret)))
  }
}
