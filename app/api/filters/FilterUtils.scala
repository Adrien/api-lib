package api.filters

import com.github.nscala_time.time.Imports.{DateTime, richDateTime, richInt}
import play.api.{Logger, Play}
import play.api.Play.current
import utils.{Crypto, DateUtils}
import scala.concurrent.Future
import play.api.mvc.{RequestHeader, Request}

object FilterUtils {

  def buildDateHeader(): String = {
    DateUtils.dateToISO8601(DateTime.now)
  }

  def buildAuthorizationHeader(toSign: String, apiKey: String, apiSecret: String): String = {
    val signature = Crypto.sign(apiKey + toSign, apiSecret)

    s"PTP:$apiKey:$signature"
  }

  //~~~~~~~~~~

  def getKeyFromRequest(request: Request[_]): Option[String] = {
    getKeyFromRequest(request.headers.get("Authorization").getOrElse(""))
  }

  def getKeyFromRequest(request: RequestHeader): Option[String] = {
    getKeyFromRequest(request.headers.get("Authorization").getOrElse(""))
  }
  private def getKeyFromRequest(security_header:String): Option[String] = {
    security_header.split(":") match {
      case Array(_, key, _) => Some(key)
      case _ => None
    }
  }

  def checkAuthorization(authorizationHeader: String, dateHeader: String, apiSecret: Option[String], f: String => Future[Boolean]): Future[Boolean] = {
    authorizationHeader.split(":") match {
      case Array(txt, key, signature) => {
        checkTimestamp(dateHeader) && checkSignature(key + dateHeader, signature, apiSecret) match {
          case true  => f(key)
          case false => Future.successful(false)
        }
      }
      case _ => Future.successful(false)
    }
  }

  private def checkTimestamp(timestamp: String): Boolean = {
    import utils.DateUtils._

    val past = DateTime.now - 15.minutes
    val future = DateTime.now + 15.minutes

    timestamp.to_date map { date =>
      date.isAfter(past) && date.isBefore(future)
    } getOrElse (false)
  }

  private def checkSignature(data: String, signature: String, apiSecretOpt: Option[String]): Boolean = {
    apiSecretOpt.map(
      apiSecret => Crypto.checkSignature(data, signature, apiSecret)
    ).getOrElse(false)
  }
}
