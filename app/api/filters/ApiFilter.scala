package api.filters

import scala.concurrent.Future
import com.google.common.io.BaseEncoding
import play.api.Play.current
import play.api.libs.Codecs
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import utils.DateUtils
import com.github.nscala_time.time.Imports._
import play.api.Play
import play.api.i18n.Messages

/**
 * Exemple : http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html
 */
trait ApiFilter extends Filter {

  private def checkAuthorization(request: RequestHeader): Future[Boolean] = {
    val header = request.headers.get("Authorization").getOrElse("")
    val timestamp = request.headers.get("DateTime").getOrElse("")

    val key = FilterUtils.getKeyFromRequest(request).getOrElse("")

  getApiSecret(key).map { secret =>
      FilterUtils.checkAuthorization(header, timestamp, secret, checkApiKey)
    }.flatMap(identity)
  }

  /**
   * Retrieve the secret corresponding to the api key
   */
  protected def getApiSecret(key: String): Future[Option[String]]

 /**
  * Check the access of the api key.
  * e.g. if you want to block a consumer after an amount of request or disable his access
  */
  protected def checkApiKey(key: String): Future[Boolean]

  protected def onUnauthorized(request: RequestHeader) = Results.Forbidden("error.api.invalid.key")

  private def apiPath = "^/v[0-9]*/.*"

  def apply(nextFilter: (RequestHeader) => Future[Result])(request: RequestHeader): Future[Result] = {
    request.path.matches(apiPath) match {
      case false => nextFilter(request)
      case true => {
        checkAuthorization(request).map {
          case false => Future { onUnauthorized(request) }
          case true  => nextFilter(request)
        }.flatMap(identity)
      }
    }
  }
}
