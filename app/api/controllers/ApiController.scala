package api.controllers

import play.api._
import play.api.mvc._
import scala.concurrent.Future
import play.api.libs.json._
import play.api.Play.current
import scala.util.Random
import FutureOptionDSL.Fail
import org.apache.commons.lang3.exception.ExceptionUtils

case class Error(technicalCode: String, message: String, code: String, stack: Option[String] = None)
case class Errors(hasError:Boolean = true, errors:List[Error])
case class StackTrace(t: Throwable, trace: String)

object ErrorManagers {
  def apply(technicalMsg: String, humanMsg: String): Error = Error(technicalMsg, humanMsg, randomErrorId())

  private def randomErrorId(): String = {
    "#" + Random.alphanumeric.take(8).mkString
  }

  implicit val errorFormat: Format[Error] = Json.format[Error]
  implicit val errorsFormat: Format[Errors] = Json.format[Errors]
}

object ApiController {

  import ErrorManagers._

  def json2result(data: Seq[(String, JsValue)]): Result = {
    Results.Status(200).apply(JsObject(data))
  }

  def formatResult(jsonResult: JsValue, jsonError: List[JsValue]): JsValue = {
    jsonError.length match {
      case 0 => jsonResult
      case _ => errorBuilder(jsonError)
    }
  }

  def formatResult(jsonResult: JsValue, errors: List[Error], debugEnable:Boolean): JsValue = {
    val errors_with_env = errors.map{ err =>
      debugEnable match {
        case true  => err
        case false => err.copy(stack = None)
      }
    }

    errors.length match {
      case 0 => jsonResult
      case _ => Json.toJson(Errors(true, errors_with_env))
    }
  }

  private def errorBuilder(errors: List[JsValue]) = {
    JsObject(Seq(
      "hasError" -> Json.toJson(errors.length > 0),
      "errors" -> flattenArray(errors)
    ))
  }

  private def flattenArray(errors: List[JsValue]): JsArray = {
    errors match {
      case List(alreadyAnArray: JsArray) => alreadyAnArray
      case _                             => JsArray(errors)
    }
  }
}

trait ApiController extends Controller {

  def translateError(msg: String): String

  /**
    * Base function to generate error in json.
    * The format is
    *
    * {
    *   hasError: true
    *   errors: [
    *     technicalMsg : "xxx",
    *     humanMsg: "xxx",
    *     code: "#12345678"
    *   ]
    * }
    */
  private def toResult(jsonResult:JsValue = JsObject(List()), errors: List[Error] = List(), status: Int)(implicit request: ApiRequest[_]):Result = {
    val data = ApiController.formatResult(jsonResult, errors, request.debugEnable)
    Status(status).apply(data).withHeaders(debugInfoOrDefault: _*).withHeaders(metadataOrDefault: _*)
  }

  def Ok(jsonResult: JsValue, status: Int)(implicit request: ApiRequest[_]): Result = {
    toResult(
      jsonResult = jsonResult,
      status = status
    )
  }

  /**
    * This is the only exception to the classic format :
    *
    * {
    *   hasError: true
    *   errors: [
    *     <js_object>
    *   ]
    * }
    *
    * We need it to take care of the json / form parsing error in Play.
    */
  def Error(jsonError: List[JsValue], status: Int)(implicit request: ApiRequest[_]): Result = {
    val data = ApiController.formatResult(JsObject(List()), jsonError)
    Status(status).apply(data).withHeaders(debugInfoOrDefault: _*).withHeaders(metadataOrDefault: _*)
  }

  def Error(fail: Fail, status: Int)(implicit request: ApiRequest[_]): Result = {
    val stackTrace: Option[StackTrace] = fail.getRootException().map(exception => StackTrace(exception, fail.userMessage())).orElse(None)
    handleError(fail.message, stackTrace, status)
  }

  def Error(errorMsg: String, status: Int)(implicit request: ApiRequest[_]): Result = {
    handleError(errorMsg, None, status)
  }

  def Error(t: Throwable, errorMsg: String, status: Int)(implicit request: ApiRequest[_]): Result = {
    val stackTrace = StackTrace(t, s"${errorMsg} ${t.getMessage} ${ExceptionUtils.getStackTrace(t)}")
    handleError(errorMsg, Some(stackTrace), status)
  }

  private def handleError(errorMsg: String, stackTrace: Option[StackTrace], status: Int)(implicit request: ApiRequest[_]): Result = {
    val error = ErrorManagers(errorMsg, translateError(errorMsg))
      .copy(stack = stackTrace.map(_.trace))

    printStackTrace(error.code, stackTrace)

    toResult(errors = List(error), status = status)
  }

  private def printStackTrace(code: String, stackTrace: Option[StackTrace]) = {
    stackTrace.map(sTrace => Logger.error(s"${code}: ${sTrace.trace}", sTrace.t))
  }

  private def debugInfoOrDefault(implicit request: ApiRequest[_]): Seq[(String, String)] = {
    request.debugEnable match {
      case false => Seq()
      case true  => requestInfo()
    }
  }

  private def requestInfo()(implicit request: ApiRequest[_]): Seq[(String, String)] = {
    val requestEndTime = System.currentTimeMillis()
    Seq(
      "debug-request-time" -> request.requestStartTime.toString,
      "debug-response-time" -> requestEndTime.toString,
      "debug-time-on-server" -> (requestEndTime - request.requestStartTime).toString,
      "debug-env" -> Play.mode.toString()
    )
  }

  private def metadataOrDefault()(implicit request: ApiRequest[_]): Seq[(String, String)] = {
    request.metaEnable match {
      case false => Seq()
      case true  => metadata
    }
  }

  private def metadata()(implicit request: ApiRequest[_]): Seq[(String, String)] = {
    Seq(
      "meta-version" -> request.api_version,
      "meta-endpoint" -> request.endPoint
    )
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class ApiRequest[A](val api_version: String, val endPoint: String, val requestStartTime: Long,
                    val debugEnable: Boolean, val metaEnable: Boolean, request: Request[A]) extends WrappedRequest[A](request)

object ApiAction extends ActionBuilder[ApiRequest] with ActionTransformer[Request, ApiRequest] {

  def transform[A](request: Request[A]): Future[ApiRequest[A]] = {
    val data = extractDataFromPath(request.path)
    val version = data._1
    val endPoint = data._2

    val debugEnable = request.headers.get("Info-Debug-Enabled").map(s => s.toBoolean).getOrElse(false)
    val metaEnable = request.headers.get("Info-Meta-Enabled").map(s => s.toBoolean).getOrElse(true)

    Future.successful(new ApiRequest(version, endPoint, System.currentTimeMillis(), debugEnable, metaEnable, request))
  }

  private def extractDataFromPath(path: String) = {
    if (path.length() < 2) {
      ("0", "")
    } else if (!path.matches(".*/v[1-9]*/.*")) {
      val version = Play.configuration.getString("api.version").getOrElse("0")
      val endPoint = path
      (version, endPoint)
    } else {
      val subPath = path.substring(2)
      val version = subPath.substring(0, subPath.indexOf('/'))
      val endPoint = subPath.substring(version.length())
      (version, endPoint)
    }
  }
}
