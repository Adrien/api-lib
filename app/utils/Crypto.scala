package utils

import com.google.common.io.BaseEncoding

object Crypto {

  def toBase64(data: String): String = {
    BaseEncoding.base64().encode(data.getBytes("utf-8"))
  }

  def sign(data: String, secret: String): String = {
    val toSign = secret + data
    val sign = play.api.libs.Crypto.sign(toSign, secret.getBytes("utf-8"))
    toBase64(sign)
  }

  def checkSignature(data: String, signature: String, secret: String): Boolean = {
    val toSign = secret + data

    val sign = play.api.libs.Crypto.sign(toSign, secret.getBytes("utf-8"))
    val resign = toBase64(sign)

    resign == signature
  }
}
