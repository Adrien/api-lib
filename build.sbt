name := """Api-Lib"""

version := "1.6.5"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  ws,
  "org.scalatest"          %% "scalatest"         % "2.2.4"  % "test"  withSources(),
  "com.papertrailapp"      %  "logback-syslog4j"  % "1.0.0"            withSources(),
  "com.wordnik"            %% "swagger-play2"     % "1.3.12"           withSources(),
  "com.github.nscala-time" %% "nscala-time"       % "1.6.0"            withSources(),
  "org.scalaz"             %% "scalaz-core"       % "7.1.5"            withSources(),
  "org.scala-lang"         %  "scala-reflect"     % scalaVersion.value
)

// code coverage
scoverage.ScoverageSbtPlugin.ScoverageKeys.coverageExcludedPackages := "<empty>;Reverse.*;.*AuthService.*;models\\.data\\..*;views.html.*"
