package com.particeep


import org.scalatest._
import slick.CopyMacro

import scala.language.postfixOps

class CopyMacroTest extends FlatSpec with Matchers {

  "Copy Macro" should "copy same type from different package" in {

    val user = com.particeep.test.TestUser("toto", "toto@gmail.com")

    val copy = CopyMacro.copyCaseClass[com.particeep.test.TestUser, com.particeep.test2.TestUser]
    val user_copied = copy(user, com.particeep.test2.TestUser())

    user_copied.name  shouldBe "toto"
    user_copied.email shouldBe "toto@gmail.com"
  }

}


